     _   _           _                   ____  _ _   _                _        _   
    | | | | ___  ___| |_    ___  _ __   | __ )(_) |_| |__  _   _  ___| | _____| |_ 
    | |_| |/ _ \/ __| __|  / _ \| '_ \  |  _ \| | __| '_ \| | | |/ __| |/ / _ \ __|
    |  _  | (_) \__ \ |_  | (_) | | | | | |_) | | |_| |_) | |_| | (__|   <  __/ |_ 
    |_| |_|\___/|___/\__|  \___/|_| |_| |____/|_|\__|_.__/ \__,_|\___|_|\_\___|\__|
                                                                               
Just a test how to host a simple website on Bitbucket Cloud. Read more [here](https://confluence.atlassian.com/bitbucket/publishing-a-website-on-bitbucket-cloud-221449776.html)

Used an excellent free 'Coming soon' template by [**In-House** Success Agency](http://www.successagency.com/in/)

To be continued!